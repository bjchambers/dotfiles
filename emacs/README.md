Keybindings
-----------

In all modes:

  - `C-c c` is bound to the most logical "compile" operation
  - `C-c t` is bound to a test variant of compile, if appropriate,
    or the same as `C-c c` otherwise.
  - `C-c C-c` is bound to the equivalent of recompile (execute the
    the previous operation).
  - `C-c o` is bound to org-mode capture
       
