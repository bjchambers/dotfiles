(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-ghc-show-info t)
 '(custom-safe-themes
   (quote
    ("4f5bb895d88b6fe6a983e63429f154b8d939b4a8c581956493783b2515e22d6d" default)))
 '(haskell-compile-cabal-build-command "cd %s && stack test --ghc-options=-ferror-spans")
 '(haskell-process-args-ghci
   (quote
    ("ghci" "--with-ghc" "intero" "--no-load" "--no-build")))
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-path-ghci "stack")
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-process-type (quote ghci))
 '(haskell-process-use-ghci t)
 '(package-selected-packages
   (quote
    (meson-mode clang-format irony irony-mode flymake-rust rust-mode cmake-mode cmake-ide tide typescript-mode counsel go-mode haskell-mode exec-path-from-shell which-key use-package smartparens smart-mode-line rainbow-delimiters projectile pandoc-mode org-bullets markdown-mode magit latex-preview-pane intero hungry-delete company-coq column-enforce-mode browse-kill-ring auctex ample-theme)))
 '(safe-local-variable-values
   (quote
    ((projectile-project-test-cmd . "./build/test/tests")
     (projectile-project-compilation-cmd . "make -C build")
     (projectile-enable-caching . t)
     (projectile-project-test-cmd . "cd build && make -j4 all test")
     (projectile-project-compilation-cmd . "cd build && make -j4")
     (projectile-project-compilation-cmd
      (cd build && make -j4))
     (eval init-intero-targets "Language:lib Language:tests")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 2.0))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.7))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-4 ((t (:inherit markdown-header-face :height 1.1))))
 '(markdown-inline-code-face ((t (:inherit font-lock-constant-face))))
 '(markdown-link-face ((t (:inherit link))))
 '(markdown-pre-face ((t (:inherit font-lock-constant-face)))))
