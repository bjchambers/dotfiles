# Emacs Setup

    ln -sf ~/dotfiles/emacs ~/.emacs.d 

Everything you need should be downloaded using the Emacs package manager
automatically.

# ZSH Setup

Install ZSH and download [antigen.zsh](https://github.com/zsh-users/antigen) to
`~/dotfiles/antigen/antigen.zsh`, then link the `_zshrc` configuration to
`~/.zshrc`.

    # See above link for latest antigen.zsh
    curl https://cdn.rawgit.com/zsh-users/antigen/v1.3.2/bin/antigen.zsh >
~/dotfiles/antigen/antigen.zsh

    ln -sf ~/dotfiles/_zshrc ~/.zshrc
